// INDEX
// 

$( document ).ready(function() {

    $('#Stage_jbeeb_3').css('z-index',0);

    if ( $(window).width() < 350 ){
        $('#Stage_jbeeb_3').css('left', -15+'px');
    }

    // Hamburger
    $('.btn-menu').css('left', -$('.btn-menu .list').width());

    $('.hamburger').click(function(){
        $(this).toggleClass('is-active');
        $('.btn-menu').toggleClass('active');

        if($('.btn-menu').hasClass('active')){
            $('.btn-menu').css('left', 0);
        }else{
            $('.btn-menu').css('left', -$('.btn-menu .list').width());  
        }
    });

    $('.list__item a').click(function(){
        $('.hamburger').toggleClass('is-active');
        $('.btn-menu').toggleClass('active');

        if($('.btn-menu').hasClass('active')){
            $('.btn-menu').css('left', 0);
        }else{
            $('.btn-menu').css('left', -$('.btn-menu .list').width());  
        }
    });

    // Menu
    $(window).scroll(function(){
        windowTop = $(window).scrollTop();
        if(windowTop>0){
            $('.header').removeClass('hide').addClass('show');
        }else{
            $('.header').removeClass('show').addClass('hide');
        }
    });

    // Modal
        $('.order_request').click(function(){
            $('.modal_request').addClass('modal_active');
            $('.modal_request')
                .css("display", "flex")
                .hide()
                .fadeIn(500);
            $('html body').css('overflow','hidden');
        });

        $('.btn-close').click(function(){
            $('.modal_request').fadeOut(500);
            $('.modal_request').removeClass('modal_active');
            $('html body').css('overflow','initial');
        })

        $('.order').click(function(){
            $('.modal_order').addClass('modal_active');
            $('.modal_order')
                .css("display", "flex")
                .hide()
                .fadeIn(500);
            $('html body').css('overflow','hidden');
        });

        $('.btn-close').click(function(){
            $('.modal_order').fadeOut(500);
            $('.modal_order').removeClass('modal_active');
            $('html body').css('overflow','initial');
        })

    // tel
        $(".tel").mask("+7(999) 999-99-99");

    // anchor
        $('a[href*="#"]').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 1000);
                return false;
            }
            }
        });
})