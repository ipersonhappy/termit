var gulp = require('gulp'),
  	pug = require('gulp-pug'), // compile jade
  	plumber = require('gulp-plumber'), // catch error
    ext_replace = require('gulp-ext-replace'), // change extension
    scss = require('gulp-sass'), // compile sass
    concat = require('gulp-concat'), // concat files
    changed = require('gulp-changed'), // only changed files
    minifyJs = require('gulp-minify'), // minify js
    postcss = require('gulp-postcss'), // postcss
    gcmq = require('gulp-group-css-media-queries'),
    browserSync = require('browser-sync').create(), // autoreload
    replace = require('gulp-replace'), // compile php
    cssnano = require('gulp-cssnano'); // minify css

// POSTCSS PLUGINS
  var processors = [
      require('postcss-position'),
      require('postcss-font-awesome'),
      require('autoprefixer'),
      require('postcss-flexibility')
  ];

// BROWSERSYNC 
  gulp.task('serve', ['html','scss','js'], function() {

    browserSync.init({
        server: "./app"
    });

    gulp.watch('build/pug/**/*.pug', ['html']);
    gulp.watch('build/scss/**/*.scss', ['scss']);
    gulp.watch('build/js/**/*.js', ['js']);
    gulp.watch("./app/*.html").on('change', browserSync.reload);
  });

// COPY FILES

  gulp.task('copy-files', ['copy-css', 'copy-js']); 
  
  // CSS
    // COPY CSS FILE MEDIA-QUERIES
    gulp.task('copy-mediaqueries-css', function(){
      gulp.src('bower_components/sass-mediaqueries/_media-queries.scss')
      .pipe(gulp.dest('build/scss/extra/sass-mediaqueries/'))
    });


    // COPY CSS FILE FAMILY
    gulp.task('copy-family-css', function(){
      gulp.src('bower_components/family.scss/source/src/_family.scss')
      .pipe(gulp.dest('build/scss/extra/_family.scss/'))
    });

    // COPY CSS
    gulp.task('copy-css', ['copy-family-css',
                           'copy-mediaqueries-css'], 
                           function(){
      gulp.dest('build/scss/extra/')
    });

  // JS
    // COPY JS JQUERY 
    gulp.task('copy-jquery-js', function(){
      gulp.src('bower_components/jquery/dist/jquery.min.js')
      .pipe(gulp.dest('build/js/jquery/'))
    });

    // COPY CSS
    gulp.task('copy-js', ['copy-jquery-js'], 
                           function(){
      gulp.dest('build/js/')
    });    

// OTHER TASKS

  // HTML
    // HTML CONCAT JS
      gulp.task('js', function() {
        gulp.src(['build/js/jquery/jquery.min.js',
                  'build/js/flexibility.js',
                  'build/js/my.js'])
          .pipe(changed('./app/dist/js/'))
          .pipe(plumber())
          .pipe(concat("all.js"))
          .pipe(minifyJs())
          .pipe(gulp.dest('./app/dist/js/'))
          .pipe(browserSync.stream());
      });

    // HTML SCSS => CSS
      gulp.task('scss', function () {
        gulp.src(['build/scss/**/*.scss',
                '!./build/scss/extra/'])
          .pipe(changed('./app/dist/css/'))
          .pipe(scss().on('error', scss.logError))
          .pipe(gcmq())
          .pipe(postcss(processors))
          .pipe(concat('all.css'))
          .pipe(cssnano())
          .pipe(gulp.dest('./app/dist/css/'))
          .pipe(browserSync.stream());
      });

    // JADE => html 
      gulp.task('html', function() {
          gulp.src(['build/pug/**/*.pug',
                  '!./build/pug/include/**/*.pug',
                  '!./build/pug/mixins/*.pug',
                  '!./build/pug/layout/*.pug',
                  '!./build/pug/template/*.pug'
                  ])
          .pipe(changed('./app/'))
          .pipe(plumber())
          .pipe(pug({
              pretty: true
            })) 
          .pipe(gulp.dest('./app/'))
      });

  // PHP
    // gulp.task('php', function(){
    //   gulp.src(['build/jade/**/*.jade',
    //             '!./build/jade/mixins/*.jade',
    //             '!./build/jade/blocks/**/*.jade',
    //             '!./build/jade/contents/*.jade',
    //             '!./build/jade/type/**/*.jade'
    //           ])
    //     .pipe(replace('false', 'true'))
    //     .pipe(pug({
    //           pretty: true
    //         }))
    //     .pipe(ext_replace('.php'))     
    //     .pipe(gulp.dest('./'))
    // });

gulp.task('default', ['serve']);